FROM php:7.2-fpm-alpine

# Heavily based on:
# * https://github.com/linuxserver/docker-bookstack/blob/master/Dockerfile
# * https://github.com/solidnerd/docker-bookstack/blob/master/Dockerfile

LABEL description="Docker image for BookStack based on PHP over Alpine"
LABEL mantainer="Erus <erus@rlab.be>, HacKan <hackan@rlab.be>"
LABEL vendor="Antifa GLUG"
LABEL vendor.url="https://antifa-glug.org"
LABEL url="https://git.rlab.be...."
LABEL version="0.1.1"
LABEL license="GNU GPL v3.0+"

# Generate hashes with `sha512sum -b <file>`
ARG COMPOSER_HASH=5a465f56b483df2314cee5dc81a8e877cb607439ebc203963ecaa5e98784bf111f969b5683b5a71560f182403ddddce2f0cda342398c5d41fc46225f82cfdcf2
# PHP base image user
ARG PHP_USER=www-data

RUN echo "Installing required packages..." \
    && apk update \
    && apk add --no-cache \
        wget \
        zlib-dev \
        freetype-dev \
        libjpeg-turbo-dev \
        libpng-dev \
        tidyhtml-dev \
        git \
    && echo "Configuring PHP-FPM..." \
    && echo 'env[PATH] = /usr/local/bin:/usr/bin:/bin' >> /usr/local/etc/php-fpm.d/www.conf \
    && chown 755 /var/www/html \
    && docker-php-ext-install pdo pdo_mysql mbstring zip tidy \
    && docker-php-ext-configure gd --with-freetype-dir=usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd

RUN echo "Installing composer..." \
    && wget \
        -O /tmp/composer-setup.php \
        'https://getcomposer.org/installer' \
    && echo "$COMPOSER_HASH */tmp/composer-setup.php" | sha512sum -c \
    && php /tmp/composer-setup.php \
    && mv composer.phar /usr/local/bin/composer \
    && mkdir -p /home/${PHP_USER}/.composer \
    && chown ${PHP_USER}:${PHP_USER} /home/${PHP_USER}/.composer \
    && echo "Cleaning up..." \
    && rm -rf \
        /root/.composer \
        /tmp/*

COPY conf/php.ini /usr/local/etc/php/php.ini
COPY docker-entrypoint.ash /usr/bin/docker-entrypoint

EXPOSE 9000

USER $PHP_USER
ENTRYPOINT ["docker-entrypoint"]
CMD ["php-fpm"]

