# BookStack Docker

A simplified docker build for [BookStack](https://github.com/BookStackApp/BookStack) wiki.  
It uses the app as a git submodule, MariaDB for DB, Nginx as proxy and Redis for cache, and runs the app as a limited user.

Note that the dockerfile is *heavily* inspired in the following two:

* https://github.com/linuxserver/docker-bookstack/blob/master/Dockerfile
* https://github.com/solidnerd/docker-bookstack/blob/master/Dockerfile

Credits to them where due.

## Deploy

Clone this repo, run `git submodule init && git submodule update` to initialize the BookStack submodule, then go into the `bookstack` directory and get the latest changes: `git pull`, then check out the desired tag, i.e.: `git checkout v0.25.1`.

Once it's done, you need to **chown all of the files in `bookstack`** as the user/group number 82 (which the one that runs in the container): `chown -R 82:82 bookstack`.  
I know this sucks, but it's better on the long run than having bookstack inside the image.

### Config

Copy sample nginx configuration and edit it to suit your needs, particularly the server name: `cp conf/nginx-vhost.conf.example conf/nginx-vhost.conf`.

Copy sample php ini file and edit it to suit your needs (these defaults are production-ready): `cp conf/php.ini.example conf/php.ini`.

Copy sample environment file and edit it to suit your needs, it's pretty much self explanatory: `cp env .env`.

### Start

You are ready to launch your BookStack wiki! Run `docker-compose up -d --build` to build the image and start the containers.  
By default, nginx is listening on port 80 (change it in `docker-compose.yml` if you need).

## License

**BookStack Docker** is made by [Erus](https://git.rlab.be/erus), [HacKan](https://hackan.net) and [collaborators](https://git.rlab.be/antifaglug/bookstack-docker/graphs/master) under GNU GPL v3.0+. You are free to use, share, modify and share modifications under the terms of that [license](LICENSE).

    Copyright (C) 2019 Erus (https://git.rlab.be/erus)
    Copyright (C) 2019 HacKan (https://hackan.net)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


