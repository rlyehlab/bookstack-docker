#!/bin/ash
set -eu

echoerr() { echo "$*" 1>&2; }
bailout() {
    echoerr "$*"
    exit 1
}

cd /var/www/html

DB_PORT=${DB_PORT:-3306}

if [ ! -f ".env" ]; then
    cat > ".env" <<-EOF
# Environment
APP_ENV=production
APP_DEBUG=${APP_DEBUG:-false}
APP_KEY=${APP_KEY}

# The below url has to be set if using social auth options
# or if you are not using BookStack at the root path of your domain.
APP_URL=${APP_URL}

# Database details
DB_PORT=${DB_PORT}
DB_HOST=${DB_HOST}
DB_DATABASE=${DB_DATABASE}
DB_USERNAME=${DB_USERNAME}
DB_PASSWORD=${DB_PASSWORD}

# Cache and session
CACHE_DRIVER=${CACHE_DRIVER:-file}
SESSION_DRIVER=${CACHE_DRIVER:-file}
SESSION_SECURE_COOKIE=${SESSION_SECURE_COOKIE}
QUEUE_DRIVER=sync
REDIS_HOST=${REDIS_HOST:-null}
REDIS_PASSWORD=${REDIS_PASSWORD:-null}
REDIS_PORT=${REDIS_PORT:-6379}
REDIS_SERVERS=${REDIS_HOST}:${REDIS_PORT:-6379}:0

# Storage
STORAGE_TYPE=local

# General auth
AUTH_METHOD=standard

# External services such as Gravatar
DISABLE_EXTERNAL_SERVICES=${DISABLE_EXTERNAL_SERVICES:-false}

# Mail settings
MAIL_DRIVER=${MAIL_DRIVER}
MAIL_HOST=${MAIL_HOST}
MAIL_PORT=${MAIL_PORT}
MAIL_USERNAME=${MAIL_USERNAME}
MAIL_PASSWORD=${MAIL_PASSWORD}
MAIL_ENCRYPTION=${MAIL_ENCRYPTION}
MAIL_FROM_NAME=${MAIL_FROM_NAME}
MAIL_FROM=${MAIL_FROM}

# Listing
APP_VIEWS_BOOKS=${APP_VIEWS_BOOKS:-list}
APP_VIEWS_BOOKSHELVES=${APP_VIEWS_BOOKSHELVES:-grid}
REVISION_LIMIT=${REVISION_LIMIT:-20}

# Tags
ALLOW_CONTENT_SCRIPTS=${ALLOW_CONTENT_SCRIPTS:-false}
EOF
    sed -i "s/\'single\'/\'errorlog\'/g" config/app.php

    echoerr "Setting dir permissions for uploads..."
    chown -R www-data:www-data public/uploads && chmod -R 775 public/uploads
    chown -R www-data:www-data storage/uploads && chmod -R 775 storage/uploads
fi

composer install

echoerr "Waiting 10s for ${DB_HOST}:${DB_PORT}"
sleep 10

php artisan migrate --force
php artisan cache:clear
php artisan view:clear

echoerr "Executing: $*"
exec "$@"

